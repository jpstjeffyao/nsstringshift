//
//  ShiftEncode.m
//  COSATabbed2
//
//  Created by User on 12/3/9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ShiftEncode.h"

@implementation NSString (ShiftEncode)

- (NSString*)Shift:(int)offset {
    NSMutableString* decoded = [NSMutableString stringWithString:self];
    
    NSMutableString* DecodeStr=[[NSMutableString alloc] init];
    @try{
    for (int i=0;i<[decoded length];i++) {
        UTF16Char xx=[decoded characterAtIndex:i];
        if (xx==' ') {
            [DecodeStr appendFormat:@" "];
        }else {
            UTF16Char newChar=xx+offset;
            [DecodeStr appendFormat:@"%C",newChar];
        }
    }
    }@catch (NSException *EXP) {
        NSLog(@"NSSTRING SHIFT :%@",EXP);
        DecodeStr=nil;
    }  
    return DecodeStr;
}

@end
