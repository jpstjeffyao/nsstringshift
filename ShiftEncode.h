//
//  ShiftEncode.h
//  COSATabbed2
//
//  Created by User on 12/3/9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (ShiftEncode) 
- (NSString*)Shift:(int)offset;
@end
